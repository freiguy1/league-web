import Vue from 'vue'
import Vuex from 'vuex'
import { DateTime } from 'luxon'
import http from '@/http'

Vue.use(Vuex)

function getCookieToken () {
  const cookies = document.cookie.split('; ')
  const kvCookies = cookies.map(c => {
    const kv = c.split('=')
    return { key: kv[0], value: kv[1] }
  })
  const cookie = kvCookies.find(kv => kv.key === 'token')
  if (cookie && cookie.value !== 'null') {
    return cookie.value
  } else { return null }
}

export default new Vuex.Store({
  state: {
    baseApiUrl: 'https://league-api-stage.herokuapp.com',
    // baseApiUrl: 'http://localhost:8000',
    token: getCookieToken(),
    myTeams: [],
    myLeagues: [],
    user: null
  },
  actions: {
    setToken ({ commit }, token) {
      commit('setToken', token)
      const expiration = DateTime.utc().plus({ days: 7 })
      const cookie = `token=${token}; path=/; expires=${expiration.toHTTP()}`
      document.cookie = cookie
    },
    async loadMyInfo ({ commit, state }) {
      if (!state.token) { return }
      const client = http.getClient()
      const response = await client.get('/v0.0.1/my/info')
      commit('updateMyTeams', response.data.teams)
      commit('updateMyLeagues', response.data.leagues)
      commit('updateUser', {
        id: response.data.id,
        firstName: response.data.first_name,
        lastName: response.last_name,
        email: response.data.email
      })
    },
    clearMyInfo ({ commit }) {
      commit('updateMyTeams', [])
      commit('updateMyLeagues', [])
      commit('updateUser', null)
    }
  },
  mutations: {
    setToken (state, token) {
      state.token = token
    },
    updateMyTeams (state, myTeams) {
      state.myTeams = myTeams
    },
    updateMyLeagues (state, myLeagues) {
      state.myLeagues = myLeagues
    },
    updateUser (state, user) {
      state.user = user
    }
  },
  strict: process.env.NODE_ENV === 'development'
})
