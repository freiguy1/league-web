import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Landing'
import SignIn from '@/components/SignIn'
import Dashboard from '@/components/Dashboard'
import LeagueDetail from '@/components/league/LeagueDetail'
import ManageLeague from '@/components/league/ManageLeague'
import EditSchedule from '@/components/league/editSchedule/EditSchedule'
import LeagueSearch from '@/components/LeagueSearch'
import TeamDetail from '@/components/TeamDetail'
import Store from '@/store'

Vue.use(Router)

let router = new Router({
  base: process.env.PUBLIC_PATH || '/',
  mode: 'history',
  routes: [
    {
      path: '/signin',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter (to, from, next) {
        if (!Store.state.token) {
          next({
            name: 'SignIn',
            query: {
              redirectTo: to.path
            }
          })
        } else {
          next()
        }
      }
    },
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/leagues/search',
      name: 'LeagueSearch',
      component: LeagueSearch
    },
    {
      path: '/leagues/:id/manage',
      name: 'ManageLeague',
      component: ManageLeague
    },
    {
      path: '/leagues/:id/editSchedule',
      name: 'EditSchedule',
      component: EditSchedule
    },
    {
      path: '/leagues/:id/',
      name: 'LeagueDetail',
      component: LeagueDetail,
      children: [
        {
          path: 'teams',
          name: 'LeagueDetail.Teams'
        },
        {
          path: 'schdule',
          name: 'LeagueDetail.Schedule'
        }
      ]
    },
    {
      path: '/teams/:id',
      name: 'TeamDetail',
      component: TeamDetail
    }
  ]
})

export default router
