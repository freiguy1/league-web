// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Store from '@/store'
import GoogleAuth from 'vue-google-auth'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate)
Vue.use(GoogleAuth, {
  client_id: '389829903885-rqedqc61glpune0hbh6ajf9rqp67295d.apps.googleusercontent.com'
})
Vue.googleAuth().load()

Vue.config.productionTip = false
Vue.use(BootstrapVue)

/* eslint-disable no-new */
const vue = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  store: Store,
  methods: {
    async init () {
      await this.$store.dispatch('loadMyInfo')
      if (this.$store.state.token && this.$route.path === '/') {
        this.$router.push({ name: 'Dashboard' })
      }
    }
  }
})

vue.init()
