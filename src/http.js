import axios from 'axios'
import Store from '@/store'

export default {
  getClient () {
    var instance = axios.create({
      baseURL: Store.state.baseApiUrl,
      timeout: 5000,
      headers: Store.state.token ? { 'Authorization': `Bearer ${Store.state.token}` } : null
    })
    return instance
  }
}
