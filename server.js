// server.js
// this is for use in production. It only hosts the files in dist
// after running `npm run build` you can run `npm run start`

var express = require('express');
var path = require('path');
var serveStatic = require('serve-static');

app = express();
app.use(serveStatic(__dirname + "/dist"));

app.get('*', function(req, res){
  res.sendFile(__dirname + '/dist/index.html');
});

var port = process.env.PORT || 5000;
app.listen(port);

console.log('server started '+ port);